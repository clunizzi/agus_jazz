module SessionsHelper

    #Logs in the giver editor
    def log_in(editor)
        session[:editor_id] = editor.id
    end
    
    #Remembers editor in a persistent session
    def remember(editor)
        editor.remember
        cookies.permanent.signed[:editor_id] = editor.id
        cookies.permanent[:remember_token] = editor.remember_token
    end
    
    #Returns the user corresponding to the remember token cookie
    def current_editor
        if (editor_id = session[:editor_id])
            @current_editor ||= Editor.find_by(id: session[:editor_id])
        elsif (editor_id = cookies.signed[:editor_id])
            editor = Editor.find_by(id: editor_id)
            if editor && editor.authenticated?(cookies[:remember_token])
                log_in editor
                @current_editor = editor
            end
        end
    end
    
    #Returns true if the given editor is the current editor
    def current_editor?(editor)
        editor == current_editor
    end
    
    #Returns true if the editor is logged in, false otherwise
    def logged_in?
        !current_editor.nil?
    end
    
    #Forgets a persistent session
    def forget(editor)
        editor.forget
        cookies.delete(:editor_id)
        cookies.delete(:remember_token)
    end
    
    #Logs out the current editor
    def log_out
        forget(current_editor)
        session.delete(:editor_id)
        @current_editor = nil
    end
    
    #Redirects to stored location (or to the default)
    def redirect_back_or(default)
        redirect_to(session[:forwarding_url] || default)
        session.delete(:forwarding_url)
    end
    
    #Stores the URL trying to be accessed
    def store_location
        session[:forwarding_url] = request.url if request.get?
    end
    
end
