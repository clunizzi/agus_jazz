class EditorsController < ApplicationController
  before_action :logged_in_editor
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user,   only: :destroy
  
  def show
    @editor = Editor.find(params[:id])
  end
  
  def index
    @editors = Editor.all
  end
  
  def new
    @editor = Editor.new
  end
  
  def create
    @editor = Editor.new(editor_params)
    if @editor.save
      flash[:success] = "Editor added"
      #just for now, then will redirect to home+editor partials
      redirect_to @editor
    else
      render 'new'
    end
  end
  
  def edit
    @editor = Editor.find(params[:id])
  end
  
  def update
    @editor = Editor.find(params[:id])
    if @editor.update_attributes(editor_params)
      flash[:success] = "Profile updated"
      redirect_to @editor
    else
      render 'edit'
    end
  end
  
  def destroy
    Editor.find(params[:id]).destroy
    flash[:success] = "Editor deleted"
    redirect_to editors_url
  end
  
  private
    
    def editor_params
      params.require(:editor).permit(:name, :email, :password,
                                      :password_confirmation)
    end
    
    #Before filters
    
    def logged_in_editor
      unless logged_in?
        store_location
        flash[:danger] = "Please log in"
        redirect_to login_url
      end
    end
    
    def correct_user
      @editor = Editor.find(params[:id])
      redirect_to(root_url) unless current_editor?(@editor)
    end
    
    def admin_user
      redirect_to(root_url) unless current_editor.admin?
    end
    
end
