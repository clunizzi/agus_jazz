class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    editor = Editor.find_by(email: params[:session][:email].downcase)
    if editor && editor.authenticate(params[:session][:password])
      log_in editor
      params[:session][:remember_me] == '1' ? remember(editor) : forget(editor)
      redirect_back_or editor
    else
      flash.now[:danger] = "Invalid email/password combination"
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
  
end
