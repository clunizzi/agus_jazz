class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :set_variant
  
  private
    
    #Return the right partial based on user_agent and logged editor (if any)
    def set_variant
      return request.variant = :desktop unless browser.mobile?
    end
    
end
