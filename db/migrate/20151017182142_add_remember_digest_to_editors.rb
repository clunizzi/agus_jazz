class AddRememberDigestToEditors < ActiveRecord::Migration
  def change
    add_column :editors, :remember_digest, :string
  end
end
