Editor.create!(name:  "Luca Fattorini",
               email: "agus@agusjazz.com",
               password:              "foobarbaz",
               password_confirmation: "foobarbaz",
               admin: true)
               
5.times do |n|
    name = Faker::Name.name
    email = "example-#{n+1}@agusjazz.com"
    password = "password"
    Editor.create!(name:  name,
                   email: email,
                   password:              password,
                   password_confirmation: password)
end
