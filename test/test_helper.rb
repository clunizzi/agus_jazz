ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Returns true if a test editor is logged in.
  def is_logged_in?
    !session[:editor_id].nil?
  end
  
  #Logs in a test editor
  def log_in_as(editor, options = {})
    password = options[:password] || 'password'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, session: { email: editor.email,
                                  password: password,
                                  remember_me: remember_me }
    else
      session[:editor_id] = editor.id
    end
  end
  
  private
    
    #Returns true inside integration test
    def integration_test?
      defined?(post_via_redirect)
    end
end
