require 'test_helper'

class EditorsSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    @editor = editors(:alice)
  end

  test "invalid signup information" do
    log_in_as(@editor)
    get signup_path
    assert_no_difference 'Editor.count' do
      post editors_path, editor: { name: "",
                                   email: "user@invalid",
                                   password: "foobar",
                                   password_confirmation: "foobaz" }
    end
    assert_template 'editors/new'
  end

  test "valid signup information" do
    log_in_as(@editor)
    get signup_path
    assert_difference 'Editor.count', 1 do
      post_via_redirect editors_path, editor: { name: "Example Editor",
                                   email: "editor@good.com",
                                   password: "foobarbaz",
                                   password_confirmation: "foobarbaz" }
    end
    assert_template 'editors/show'
  end

end
