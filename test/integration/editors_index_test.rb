require 'test_helper'

class EditorsIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = editors(:alice)
    @non_admin = editors(:luca)
  end
  
  test "index as admin and delete links" do
    log_in_as(@admin)
    get editors_path
    assert_template 'editors/index'
    editors_list = Editor.all
    editors_list.each do |editor|
      assert_select 'a[href=?]', editor_path(editor), text: editor.name
      unless editor == @admin
        assert_select 'a[href=?]', editor_path(editor), text: 'delete'
      end
    end
    assert_difference 'Editor.count', -1 do
      delete editor_path(@non_admin)
    end
  end
  
  test "index as non-admin" do
    log_in_as(@non_admin)
    get editors_path
    assert_select 'a', text: 'delete', count: 0
  end

end
