require 'test_helper'

class EditorsLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @editor = editors(:alice)
  end
  
  test "login with invalid info" do
    get login_path
    assert_template 'sessions/new'
    post login_path session: { email: "", passowrd: ""}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login with valid information followed by logout" do
    get login_path
    assert_template 'sessions/new'
    post login_path session: { email: @editor.email, password: 'password' }
    assert_redirected_to @editor
    follow_redirect!
    assert_template 'editors/show'
    #I will check for the presence of certain class on the home template further
    #assert_select ".add-concert"
    #assert_select ".add-musician"
    #assert_select ".add-instrument" ...
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window.
    delete logout_path
    follow_redirect!
    #assert_select ".add-concert", count: 0
    #assert_select ".add-musician", count: 0
    #assert_select ".add-instrument", count: 0 ...
  end
  
  test "login with remembering" do
    log_in_as(@editor, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end
  
  test "login without remembering" do
    log_in_as(@editor, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
