require 'test_helper'

class EditorsEditTest < ActionDispatch::IntegrationTest

  def setup
    @editor = editors(:alice)
  end
  
  test "unsuccessful edit" do
    log_in_as(@editor)
    get edit_editor_path(@editor)
    assert_template 'editors/edit'
    patch editor_path(@editor), editor: { name: "",
                                          email: "not@valid",
                                          password: "",
                                          password_confirmation: "" }
    assert_template 'editors/edit'
  end
  
  test "successflu edit" do
    get edit_editor_path(@editor)
    log_in_as(@editor)
    assert_redirected_to edit_editor_path(@editor)
    name = "Foo Bar"
    email = "foo@bar.baz"
    patch editor_path(@editor), editor: { name: name,
                                          email: email,
                                          password: "",
                                          password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @editor
    @editor.reload
    assert_equal name, @editor.name
    assert_equal email, @editor.email
  end
end
