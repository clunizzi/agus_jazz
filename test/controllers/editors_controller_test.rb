require 'test_helper'

class EditorsControllerTest < ActionController::TestCase
  
  def setup
    @editor = editors(:alice)
    @other_editor = editors(:luca)
  end
  
  test "should get new" do
    log_in_as(@editor)
    get :new
    assert_response :success
  end
  
  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  
  test "should redirect edit when not logged in" do
    get :edit, id: @editor
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect update when not logged in" do
    patch :update, id: @editor, editor: { name: @editor.name,
                                          email: @editor.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect edit when logged in as wrong editor" do
    log_in_as(@other_editor)
    get :edit, id: @editor
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when logged in as wrong editor" do
    log_in_as(@other_editor)
    patch :update, id: @editor, editor: { name: @editor.name,
                                          email: @editor.email }
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Editor.count' do
      delete :destroy, id: @editor
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_editor)
    assert_no_difference 'Editor.count', 0 do
      delete :destroy, id: @editor
    end
    assert_redirected_to root_url
  end
end
