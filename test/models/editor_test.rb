require 'test_helper'

class EditorTest < ActiveSupport::TestCase
  def setup
    @editor = Editor.new(name: "Editor User", email: "example@email.org",
                          password: "foobar12", password_confirmation: "foobar12")
  end
  
  test "should be valid" do
    assert @editor.valid?
  end
  
  test "name should be present" do
    @editor.name = "    "
    assert_not @editor.valid?
  end
  
  test "email should be present" do
    @editor.email = "    "
    assert_not @editor.valid?
  end
  
  test "name should not be too long" do
    @editor.name = "a" * 51
    assert_not @editor.valid?
  end

  test "email should not be too long" do
    @editor.email = "a" * 244 + "@example.com"
    assert_not @editor.valid?
  end
  
  test "email validation should accept valid address" do
    valid_addresses = %w[user@example.com USER@foo.com A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @editor.email = valid_address
      assert @editor.valid?, "#{valid_addresses.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid address" do
    invalid_addresses = %w[user@example,com user_at_foo.com user.name@examoke.
                         foo@foo_bar.com foo@bar+baz.cn]
    invalid_addresses.each do |invalid_address|
      @editor.email = invalid_address
      assert_not @editor.valid?, "#{invalid_addresses.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_editor = @editor.dup
    duplicate_editor.email = @editor.email.upcase
    @editor.save
    assert_not duplicate_editor.valid?
  end
  
  test "password should be present (nonblank)" do
    @editor.password = @editor.password_confirmation = " " * 6
    assert_not @editor.valid?
  end
  
  test "password should have minimum length" do
    @editor.password = @editor.password_confirmation = "a" * 7
    assert_not @editor.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @editor.authenticated?('')
  end
  
end
