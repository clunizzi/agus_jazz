require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
    
    def setup
        @editor = editors(:alice)
        remember(@editor)
    end
    
    test "current_editor returns right editor when session is nil" do
        assert_equal @editor, current_editor
        assert is_logged_in?
    end
    
    test "current_editor returns nil when remember digest is wrong" do
        @editor.update_attribute(:remember_digest, Editor.digest(Editor.new_token))
        assert_nil current_editor
    end
end

